local PART={}
PART.ID = "timemonsterroundels"
PART.Name = "1972 TARDIS Roundels"
PART.Model = "models/karmal/72/roundels.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.Collision = true

TARDIS:AddPart(PART)

local PART={}
PART.ID = "timemonsterroundelglass"
PART.Name = "1972 TARDIS Roundel Glass"
PART.Model = "models/karmal/72/roundel_glass.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)