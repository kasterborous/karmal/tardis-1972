local PART={}
PART.ID = "timemonsterscanner"
PART.Name = "1972 TARDIS Scanner"
PART.Model = "models/karmal/72/scanner_roundel.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false

if CLIENT then
	function PART:Think()
		local power=self.exterior:GetData("power-state")
		local switch = TARDIS:GetPart(self.interior,"sharpblackknob")
		if power == true then
			if ( switch:GetOn() ) then
				self:SetMaterial("models/karmal/72/roundel_scanner_anim")
			else
			self:SetMaterial("models/karmal/72/roundel_scanner")
			end
		else
			self:SetMaterial("models/karmal/72/roundel_scanner_off")
		end
	end
end

TARDIS:AddPart(PART)