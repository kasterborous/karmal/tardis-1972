local PART={}
PART.ID = "timemonstermetalpanel"
PART.Name = "1972 TARDIS Metal Panel"
PART.Model = "models/karmal/72/metal_panel.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false

TARDIS:AddPart(PART)