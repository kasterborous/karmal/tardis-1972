local PART={}
PART.ID = "timemonsterintdoor"
PART.Name = "1972 TARDIS Interior Door"
PART.Model = "models/karmal/72/intdoor.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.28
PART.Sound = "karmal/72/intdoor.wav"
PART.SoundPos = Vector(-36.985, 148.36, 39.096)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)