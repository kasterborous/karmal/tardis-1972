local PART={}
PART.ID = "timemonsterbigdoors"
PART.Name = "1972 TARDIS Big Doors"
PART.Model = "models/karmal/72/bigdoors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.30
PART.ShouldTakeDamage = true

PART.Sound = "karmal/72/bigdoors.wav"
PART.SoundPos = Vector(-1.10, -166.15, 41.05)

if SERVER then
	function PART:Use()
		self:SetCollide(self:GetOn())
	end

	function PART:Toggle( bEnable, ply )
		sound.Play(self.Sound, self:LocalToWorld(self.SoundPos))
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
	end
end

TARDIS:AddPart(PART)

PART.ID = "timemonsterbigdoors_nonclickable"
PART.Use = false

TARDIS:AddPart(PART)