local PART={}
PART.ID = "timemonsterbaywalllights"
PART.Name = "1972 TARDIS Bay Wall Lights"
PART.Model = "models/karmal/72/baywall_lights.mdl"
PART.AutoSetup = true
PART.ShouldTakeDamage = false
PART.Collision = true

TARDIS:AddPart(PART)