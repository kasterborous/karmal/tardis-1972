TARDIS:AddControl({
	id = "timemonster_bigdoors",
	tip_text = "Main doors",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local intdoors = TARDIS:GetPart(self, "timemonsterbigdoors")
		if not IsValid(intdoors) then return end
		intdoors:Toggle(!intdoors:GetOn(), ply)
	end,
})

TARDIS:AddControl({
	id = "timemonster_intdoors",
	tip_text = "Interior doors",
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local intdoors = TARDIS:GetPart(self, "timemonsterintdoor")
		if not IsValid(intdoors) then return end
		intdoors:Toggle(!intdoors:GetOn(), ply)
	end,
})