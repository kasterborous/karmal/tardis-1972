-- 1973 TARDIS (classic doors)

local T={}
T.Base="72"
T.Name="1972 TARDIS"
T.ID="72cl"
T.EnableClassicDoors = true

T.IsVersionOf = "72"

T.Interior={
	Portal = {
		pos = Vector(-1.10, -166.15, 41.05),
		ang = Angle(0, 90, 0),
		width = 80,
		height = 200
	},
	Fallback = {
		pos = Vector(0, -141.5, 3),
		ang = Angle(0, 90, 0),
	},
	Sounds={
		Door={
			enabled=true,
			open = "karmal/72/bigdoors.wav",
			close = "karmal/72/bigdoors.wav",
		},
	},
	Parts={
		timemonsterbigdoors=false,
		intdoor = {
			model="models/karmal/72/bigdoors.mdl",
			pos = Vector(-0.132243, -157.75, 0.370476),
			--ang = Angle(0, -90, 0),
		},
		door = {
			posoffset = Vector(26.70, 1.25, -44.75),
		},
	},
	Controls = {
		sharphandlelever6b = "door",
	},
	IntDoorAnimationTime = 3.5,
}

T.Exterior = {
	Portal = {
		pos = Vector(25.5, 1, 44.75),
		ang = Angle(0, 0, 0),
		width = 40,
		height = 85
	},
	Parts = {
		door = {
			posoffset = Vector(-26.70, -1.25, -44.75),
		},
	},
}

TARDIS:AddInterior(T)