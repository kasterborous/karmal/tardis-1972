-- 1972 TARDIS

local T = {}
T.Base = "base"
T.Name = "1972 TARDIS"
T.ID = "72"

T.Versions = {
	main = {
		classic_doors_id = "72cl",
		double_doors_id = "72"
	},
	other = {
		{
			name = "Computer Exterior",
			id = "72alt",
		},
	},
}

T.Interior = {
	Model = "models/karmal/72/interior.mdl",
	Screens = {
		{
			pos = Vector(-156.575, 102.738, 95.700),
			ang = Angle(0, 45, 90),
			width = 334,
			height = 260,
			visgui_rows = 4,
			power_off_black = false
		}
	},
	ScreensEnabled = false,
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_int.wav",
			fullflight = "karmal/73/full.wav"
		},
		Power = {
			On = "karmal/73/powerup.wav", -- Power On
			Off = "karmal/73/powerdown.wav" -- Power Off
		},
		FlightLoop = "karmal/73/flight_loop.wav",
		Door = {
			enabled = {},
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	ExitDistance = 1000,
	--Sequences = "sharp_sequences",
	Parts = {
		sharpentrancedoorframe 		= {},
		timemonsterroundels 		= {},
		timemonsterroundelglass 	= {},
		timemonsterbaywalllights	= {},
		timemonsterscanner 			= {},
		timemonstermetalpanel 		= {},
		timemonsterintdoor 			= {pos = Vector(-39.4, 157.843, 1.59548), 			ang = Angle(0, -90, 0)},
		timemonsterbigdoors 		= {pos = Vector(-0.132243, -157.75, 0.370476)},

		door = {
			model = "models/karmal/73/exterior/doors.mdl",
			posoffset = Vector(27.20, 0, -44.75),
			angoffset = Angle(0, 90, 0)
		},
	},
	Controls = {
		sharphandlelever6b = "timemonster_bigdoors",
		sharpswitch6e = "timemonster_intdoors",
	},
	Portal = {
		pos = Vector(-1.10, -276.75, 40.05),
		ang = Angle(0, 90, 0),
		width = 150,
		height = 150
	},
	Fallback = {
		pos = Vector(-173, 0, 3),
		ang = Angle(0, 90, 0),
	},
	IdleSound = {
		{
			path = "karmal/72/interior.wav",
			volume = 0.1
		}
	},
	LightOverride = {
		basebrightness = 0.35,
		nopowerbrightness = 0.04
	},
	Light = {
		color = Color(255, 252, 215),
		warncolor = Color(255, 0, 0),
		pos = Vector(0.232, -0.038, 160.996),
		brightness = 1
	},

	Lights = {
		{color = Color(255, 223, 202),	pos = Vector(190,80,65),	brightness  =  0.12,	nopower = false},
--		{color = Color(255, 217, 185),	pos = Vector(0.232, -0.038, 160.996),	brightness  =  0.021,	nopower = true, warncolor = Color(60, 0, 0)},

	},

	TipSettings = {
		style = "73",
		view_range_max = 55,
		view_range_min = 40,
	},

}

T.Exterior = {
	Model = "models/karmal/73/exterior/exterior.mdl",
	Mass = 5000,
	DoorAnimationTime = 0.6,
	ScannerOffset = Vector(30, 0, 50),	
	Fallback = {
		pos = Vector(50, 0, 5),
		ang = Angle(0, 0, 0)
	},
	Light = {
		enabled = {},
		pos = Vector(0.5, 2, 107),
		color = Color(235, 235, 255)
	},
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_ext.wav"
		},
		FlightLoop = "karmal/73/flight_loopext.wav",
		Door = {
			enabled = {},
			open = "karmal/73/dooropen.wav",
			close = "karmal/73/doorclose.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	Portal = {
		pos = Vector(26, -0.15, 44.75),
		ang = Angle(0, 0, 0),
		width = 48,
		height = 85
	},
	Parts = {
		door = {
			model = "models/karmal/73/exterior/doorsext.mdl", posoffset = Vector(-27.20, 0, -44.75), angoffset = Angle(0, -90, 0), AnimateSpeed = 1
		},
		vortex = {
			model = "models/atrovis/custom/vortexes/1_vortex.mdl",
			pos = Vector(0, 0, 50),
			ang = Angle(0, 180, 0),
			scale = 10,
		}
	},
	Teleport = {
		SequenceSpeed = 0.460,
		SequenceSpeedFast = 0.535,
		DematSequence = {
			255,
			200,
			150,
			100,
			70,
			50,
			20,
			0
		},
		MatSequence = {
			0,
			20,
			50,
			100,
			150,
			188,
			255
		}
	},
}

T.Templates={
	sharp_console = {
		parts_rotation = Angle(0, 120, 0),
	},
	time_monster_console = {
		parts_rotation = Angle(0, 120, 0),
		override = true,
	},
}

T.Interior.TextureSets = {
	normal = {
		prefix = "models/karmal/",
		{ "self", 4, "72/roundel_backing_orange" },
		{ "self", 6, "72/walls_metal" },
		{ "self", 8, "73/ceiling_central_lamp" },
		{ "self", 11, "72/baylights_lamp" },
		{ "self", 15, "72/ceiling_metal" },
		{ "self", 16, "72/ceiling_lights" },

		{ "timemonsterroundels", "72/roundel_bowls" },
		{ "timemonsterbaywalllights", 1, "73/console_lamps" },
		{ "timemonsterbaywalllights", 2, "73/console_lamps_flash" },
		{ "timemonsterbaywalllights", 3, "73/console_lamps_flash_slow" },
		{ "timemonstermetalpanel", "72/wall_panel" },

		{ "timemonsterintdoor", 1, "72/roundel_bowls" },
		{ "timemonster_intdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors_nonclickable", 1, "72/roundel_bowls" },
		{ "intdoor", 1, "72/roundel_bowls" },
	},
	off = {
		prefix = "models/karmal/",
		{ "self", 4, "72/roundel_backing_orange_off" },
		{ "self", 6, "72/walls_metal_off" },
		{ "self", 8, "73/ceiling_central_lamp_off" },
		{ "self", 11, "72/baylights_lamp_off" },
		{ "self", 15, "72/ceiling_metal_off" },
		{ "self", 16, "72/ceiling_lights_off" },

		{ "timemonsterroundels", "72/roundel_bowls_off" },
		{ "timemonsterbaywalllights", 1, "73/console_lamps_off" },
		{ "timemonsterbaywalllights", 2, "73/console_lamps_off" },
		{ "timemonsterbaywalllights", 3, "73/console_lamps_off" },
		{ "timemonstermetalpanel", "72/wall_panel_off" },

		{ "timemonsterintdoor", 1, "72/roundel_bowls_off" },
		{ "timemonster_intdoors", 1, "72/roundel_bowls_off" },
		{ "timemonsterbigdoors", 1, "72/roundel_bowls_off" },
		{ "timemonsterbigdoors_nonclickable", 1, "72/roundel_bowls_off" },
		{ "intdoor", 1, "72/roundel_bowls_off" },
	},
	warning = {
		prefix = "models/karmal/",
		{ "self", 4, "72/roundel_backing_warning" },
		{ "self", 6, "72/walls_metal" },
		{ "self", 8, "73/ceiling_central_lamp" },
		{ "self", 11, "72/baylights_lamp" },
		{ "self", 15, "72/ceiling_metal_warning" },
		{ "self", 16, "72/ceiling_lights_warning" },

		{ "timemonsterroundels", "72/roundel_bowls" },
		{ "timemonsterbaywalllights", 1, "73/console_lamps" },
		{ "timemonsterbaywalllights", 2, "73/console_lamps_flash" },
		{ "timemonsterbaywalllights", 3, "73/console_lamps_flash_slow" },
		{ "timemonstermetalpanel", "72/wall_panel_warning" },

		{ "timemonsterintdoor", 1, "72/roundel_bowls" },
		{ "timemonster_intdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors_nonclickable", 1, "72/roundel_bowls" },
		{ "intdoor", 1, "72/roundel_bowls" },
	},
	flight  = {
		prefix = "models/karmal/",
		{ "self", 4, "72/roundel_backing_orange_flight" },
		{ "self", 6, "72/walls_metal" },
		{ "self", 8, "73/ceiling_central_lamp" },
		{ "self", 11, "72/baylights_lamp" },
		{ "self", 15, "72/ceiling_metal" },
		{ "self", 16, "72/ceiling_lights_flight" },

		{ "timemonsterroundels", "72/roundel_bowls" },
		{ "timemonsterbaywalllights", 1, "73/console_lamps" },
		{ "timemonsterbaywalllights", 2, "73/console_lamps_flash" },
		{ "timemonsterbaywalllights", 3, "73/console_lamps_flash_slow" },
		{ "timemonstermetalpanel", "72/wall_panel" },

		{ "timemonsterintdoor", 1, "72/roundel_bowls" },
		{ "timemonster_intdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors_nonclickable", 1, "72/roundel_bowls" },
		{ "intdoor", 1, "72/roundel_bowls" },
	},
	warning_flight = {
		prefix = "models/karmal/",
		{ "self", 4, "72/roundel_backing_warning_flight" },
		{ "self", 6, "72/walls_metal" },
		{ "self", 8, "73/ceiling_central_lamp" },
		{ "self", 11, "72/baylights_lamp" },
		{ "self", 15, "72/ceiling_metal_warning" },
		{ "self", 16, "72/ceiling_lights_warning_flight" },

		{ "timemonsterroundels", "72/roundel_bowls" },
		{ "timemonsterbaywalllights", 1, "73/console_lamps" },
		{ "timemonsterbaywalllights", 2, "73/console_lamps_flash" },
		{ "timemonsterbaywalllights", 3, "73/console_lamps_flash_slow" },
		{ "timemonstermetalpanel", "72/wall_panel_warning" },

		{ "timemonsterintdoor", 1, "72/roundel_bowls" },
		{ "timemonster_intdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors", 1, "72/roundel_bowls" },
		{ "timemonsterbigdoors_nonclickable", 1, "72/roundel_bowls" },
		{ "intdoor", 1, "72/roundel_bowls" },
	},
}

local TEXTURE_UPDATE_DATA_IDS = {
	["power-state"] = true,
	["health-warning"] = true,
	["teleport"] = true,
	["vortex"] = true,
	["flight"] = true,
}

T.CustomHooks = {
	travel_textures = {
		exthooks = {
			["DataChanged"] = true,
		},
		func = function(ext, int, data_id, data_value)
			if not TEXTURE_UPDATE_DATA_IDS[data_id] then return end

			prefix = "models/cem/toyota_smith/"

			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning")
			local teleport = ext:GetData("teleport")
			local flight = ext:GetData("flight")
			local vortex = ext:GetData("vortex")

			if not power then
				int:ApplyTextureSet("off")
			else
				if flight or teleport or vortex then
					int:ApplyTextureSet(warning and "warning_flight" or "flight")
				else
					int:ApplyTextureSet(warning and "warning" or "normal")
				end
			end
		end,
	},
}

TARDIS:AddInterior(T)