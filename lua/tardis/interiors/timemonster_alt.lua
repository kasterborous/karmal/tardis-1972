-- 1972 TARDIS

local T = {}
T.Base = "72"
T.IsVersionOf = "72"
T.ID = "72alt"

T.EnableClassicDoors = true

T.Interior = {
	Parts = {
		timemonsterbigdoors = false,
		timemonsterbigdoors_nonclickable = {pos = Vector(-0.132243, -157.75, 0.370476)},
		sharpentrancedoorframe 		= {},
		timemonsterroundels 			= {},
		timemonsterroundelglass 			= {},
		timemonstermetalpanel 		= {},
		timemonsterintdoor 		= false,
		intdoor = {
			model = "models/karmal/72/intdoor.mdl",
			pos = Vector(-39.4, 157.843, 1.59548),
			ang = Angle(0, -90, 0),
		},

		door = {
			model = "models/karmal/72/exterior/exterior_computer_door.mdl",
			posoffset = Vector(27.20, 0, -47.30),
			angoffset = Angle(0, 0, 0),
			use_exit_point_offset = true,
		},
	},
	Controls = {
		sharphandlelever6b = "door",
	},
	Portal = {
		pos = Vector(-4, 203.545, 45.05),
		ang = Angle(0, 180, 0),
		width = 61,
		height = 138,

		exit_point_offset = {
			pos = Vector(-36, -42, 0),
			ang = Angle(0, 90, 0),
		},
	},
	Sounds={
		Door={
			enabled=true,
			open = "karmal/72/intdoor.wav",
			close = "karmal/72/intdoor.wav",
		},
	},
	Fallback = {
		pos = Vector(-38.098, 145.17, 4.037),
		ang = Angle(0, 90, 0),
	},
	IntDoorAnimationTime = 4,
}

T.Exterior = {
	Model = "models/karmal/72/exterior/exterior_computer.mdl",
	Mass = 5000,
	DoorAnimationTime = 2,
	ScannerOffset = Vector(30, 0, 50),
	Fallback = {
		pos = Vector(30, 50, 5),
		ang = Angle(0, 0, 0)
	},
	Light = {
		enabled = false,
	},
	ProjectedLight = {
		color = Color(0,0,0),
		warncolor = Color(0,0,0),
		offset = Vector(-20,40,51.1),
	},
	Sounds = {
		Teleport = {
			demat = "karmal/73/demat.wav",
			mat = "karmal/73/mat.wav",
			demat_fail = "karmal/73/demat_fail_ext.wav"
		},
		FlightLoop = "karmal/73/flight_loopext.wav",
		Door = {
			enabled = {},
			open = "karmal/72/computerdoor.wav",
			close = "karmal/72/computerdoor.wav"
		},
		Lock = "karmal/73/lock.wav"
	},
	Portal = {
		pos = Vector(0, 27.15, 47.50),
		ang = Angle(0, 90, 0),
		width = 46,
		height = 89,

		exit_point_offset = {
			pos = Vector(20, 30, 1),
			ang = Angle(0, -90, 0),
		},
	},
	Parts = {
		door = {
			model = "models/karmal/72/exterior/exterior_computer_door.mdl", posoffset = Vector(-27.15, 0, -47.50), angoffset = Angle(0, 180, 0), AnimateSpeed = 0.3
		},
	},
}

T.Templates={
	sharp_console = {
		parts_rotation = Angle(0, 120, 0),
	},
	time_monster_console = {
		parts_rotation = Angle(0, 120, 0),
		override = true,
	},
}
TARDIS:AddInterior(T)